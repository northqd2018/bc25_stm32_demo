
#ifndef __AT_CMD_H__
#define __AT_CMD_H__

#include "at.h"

#ifdef __cplusplus
extern "C" {
#endif

#define AT_OK			"OK"
#define AT_TEST			"AT"
#define AT_QRST			"AT+QRST=1"
#define AT_QPOWD 		"AT+QPOWD=%d"
#define AT_CGSN     	"AT+CGSN=%d"
#define ATE				"ATE0"
#define AT_CPIN         "AT+CPIN?"
#define AT_IPADDR		"AT+CGPADDR"
#define AT_IPADDR_SUCC  "+CGPADDR: 1,"
#define AT_CSQ          "AT+CSQ"
#define AT_QCSEARFCN    "AT+QCSEARFCN=0"
#define AT_CESQ   		"AT+CESQ"
#define AT_QENG   		"AT+QENG=0"
#define AT_CFUN     	"AT+CFUN=%d"
#define AT_QSCLK      	"AT+QSCLK=%d"
#define AT_CPSMS      	"AT+CPSMS=%d"
#define AT_CEDRXS     	"AT+CEDRXS=%d"


//	+QENG: 0,<sc_earfcn>,<sc_earfcn_offset>,<sc_pci>,
//	<sc_cellid>,[<sc_rsrp>],[<sc_rsrq>],[<sc_rssi>],
//	[<sc_snr>],<sc_band>,<sc_tac>,[<sc_ecl>],[<sc_tx_pwr>]

// +QENG: 0,2504,,9,"6902759",-86.15,28,-73,20,5,"69bd",0,13
typedef struct
{
	int sc_earfcn;			// 整型。服务小区的 EARFCN。范围：0-262143。
	int sc_earfcn_offset;	// 整型。服务小区的 EARFCN 偏移量。
	int sc_pci;				// 整型。服务小区的物理小区 ID，范围：0-503。
	char sc_cellid[10];		// 字符串型。4 字节（28 位）服务小区 ID。十六进制格式。.
	float sc_rsrp;			// 带符号的小数型。服务小区的 RSRP 值。单位：dBm。（可为负数值）。仅在 RRC-IDLE 状态下可用。
	int sc_rsrq;			// 有符号整型。服务小区的 RSRQ 值。单位：dB。（可为负数值）。仅在RRC-IDLE 状态下可用。
	int sc_rssi;			// 有符号整型。表示服务小区的 RSSI 值。单位：dBm。（可为负数值）。仅在RRC-IDLE状态下可用。
	int sc_snr;				// 有符号整型。表示服务小区的上一个 SNR 值。单位：dB。仅在 RRC-IDLE状态下可用。
	int sc_band;			// 整型。当前服务小区频段。
	char sc_tac[2];			// 字符串型。双字节跟踪区域代码（TAC）。十六进制格式（例如，“00C3” 等于十进制格式的 195）。
	int sc_ecl;				// 整型。服务小区的上次增强覆盖级别（ECL）值。范围：0-2。
	int sc_tx_pwr;			// 有符号整型。当前 UE 发射功率。 单位：dBm。
	
}at_qeng_info_t;

typedef enum {			
	
	QSCLK_DISABLE = 0,
	QSCLK_ENABLE = 1
	
}enum_psm_mode_t;

typedef enum {
	
	CPSMS_DISABLE = 0,
	CPSMS_ENABLE = 1
}enum_cpsms_mode_t;

typedef enum {
	
	CEDRXS_DISABLE = 0,
	CEDRXS_ENABLE = 1
}enum_cedrxs_mode_t;

typedef enum {

	EMERGENCY_MODE = 0,
	NORMAL_MODE,
	RESET_MODE
}enum_qpwd_mode_t;

typedef enum {
	
	SN_SNT = 0,
	IMEI_SNT = 1
}enum_cgsn_mode_t;

typedef enum {
	
	CFUN_DISABLE = 0,
	CFUN_ENABLE = 1
}enum_cfun_mode_t;

int8_t at_query_cgsn(enum_cgsn_mode_t cgsn_snt);
int8_t at_turn_off_echo(void);
int8_t at_query_cpin(void);
int8_t at_set_qsclk(enum_psm_mode_t psm_mode);
int8_t at_set_cpsms(enum_cpsms_mode_t cpsms_mode);
int8_t at_set_cedrxs(enum_cedrxs_mode_t cedrxs_mode);
int8_t at_set_clean_frequency(void);
int8_t at_query_qeng(at_qeng_info_t* qeng_info);
int8_t at_set_cfun(enum_cfun_mode_t cfun_mode);
int8_t at_query_cgpaddr(void);

#ifdef __cplusplus
}
#endif

#endif /* __AT_CMD_H__*/
