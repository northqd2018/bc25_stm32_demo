/****************************************************************************
*
* Copy right: 2020-, Copyrigths of Quectel Ltd.
****************************************************************************
* File name: at.c
* History: Rev1.0 2020-12-1
****************************************************************************/

#include "at.h"
#include "ringbuffer.h"
#include "dbg_log.h"
#include "middleware.h"

#include "usart.h"

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

static char g_send_buf[AT_CMD_MAX_LEN] = {0};
static uint16_t g_last_cmd_len = 0;

static uint8_t g_uart_rx_buf[BUFF_SIZE_MAX] = {0};  // uart ringbuffer内存空间

struct ringbuffer g_uart_rxcb;
static at_info_t g_at_info = {0};    
static at_response_t g_at_resp;

/**
 * @description: 用于测试使用
 * @param: none
 * @return: none
 */
void test_unit(void)
{
    ringbuffer_init(&g_uart_rxcb, g_uart_rx_buf, BUFF_SIZE_MAX);
}

/**
 * @description: at相关初始化
 * @param: none
 * @return: none
 */
void at_init(void)
{
    /* 初始化 */
    ringbuffer_init(&g_uart_rxcb, g_uart_rx_buf, BUFF_SIZE_MAX);

    g_at_info.recv_bufsz = BUFF_SIZE_MAX;
    g_at_info.recv_line_len = 0;
    memset(g_at_info.recv_line_buf,0,g_at_info.recv_bufsz);
    g_at_info.lock = 0;
    g_at_info.resp_notice = 0;
    g_at_info.urc_table = NULL;
    g_at_info.urc_table_size = 0;

    LOG_D("at_init is ok.\r\n");
}

/**
 * @description: at相关初始化
 * @param: urc_table urc匹配表
 * @param: table_sz 匹配表大小
 * @return: 执行结果码
 */
int at_set_urc_table(const struct at_urc *urc_table, uint16_t table_sz)
{
    uint16_t idx;

    for (idx = 0; idx < table_sz; idx++)
    {
        assert_param(urc_table[idx].cmd_prefix);
        assert_param(urc_table[idx].cmd_suffix);
    }

    if ( 0 == g_at_info.urc_table_size )
    {
        g_at_info.urc_table = (struct at_urc_table *) calloc(1, sizeof(struct at_urc_table));
        if ( NULL == g_at_info.urc_table )
        {
            return Q_ENOMEM;
        }

        g_at_info.urc_table[0].urc = urc_table;
        g_at_info.urc_table[0].urc_size = table_sz;
        g_at_info.urc_table_size++;
    }
    else
    {
        struct at_urc_table *old_urc_table = NULL;
        uint32_t old_table_size = g_at_info.urc_table_size * sizeof(struct at_urc_table);

        old_urc_table = (struct at_urc_table *) malloc(old_table_size);
        if ( NULL == old_urc_table)
        {
            return Q_ENOMEM;
        }
        memcpy(old_urc_table, g_at_info.urc_table, old_table_size);

        /* realloc urc table space */
        g_at_info.urc_table = (struct at_urc_table *) realloc(g_at_info.urc_table,
                                   old_table_size + sizeof(struct at_urc_table));
        if ( NULL == g_at_info.urc_table )
        {
            free(old_urc_table);
            return Q_ENOMEM;
        }
        memcpy(g_at_info.urc_table, old_urc_table, old_table_size);
        g_at_info.urc_table[g_at_info.urc_table_size].urc = urc_table;
        g_at_info.urc_table[g_at_info.urc_table_size].urc_size = table_sz;
        g_at_info.urc_table_size++;

        free(old_urc_table);
    }
    return Q_EOK;
}

/**
 * @description: 匹配相应的URC
 * @param: none
 * @return: 返回查询到的urc结果
 */
static const struct at_urc *get_urc_obj(void)
{
    uint16_t i, j, prefix_len, suffix_len;
    uint16_t bufsz;
    char *buffer = NULL;
    const struct at_urc *urc = NULL;
    struct at_urc_table *urc_table = NULL;

    if ( NULL == g_at_info.urc_table)
    {
        return NULL;
    }

    buffer = g_at_info.recv_line_buf;
    bufsz = g_at_info.recv_line_len;

    for (i = 0; i < g_at_info.urc_table_size; i++)
    {
        for (j = 0; j < g_at_info.urc_table[i].urc_size; j++)
        {
            urc_table = g_at_info.urc_table + i;
            urc = urc_table->urc + j;

            prefix_len = strlen(urc->cmd_prefix);
            suffix_len = strlen(urc->cmd_suffix);

            if (bufsz < prefix_len + suffix_len)
            {
                continue;
            }

            if ((prefix_len ? !strncmp(buffer, urc->cmd_prefix, prefix_len) : 1)
                    && (suffix_len ? !strncmp(buffer + bufsz - suffix_len, urc->cmd_suffix, suffix_len) : 1))
            {
                return urc;
            }
        }
    }
    return NULL;
}
/**
 * @description: 从rx_ringbuffer中获取数据
 * @param: buf 待存放buffer
 * @param: size 指定获取数据大小
 * @return: 执行结果码
 */
int8_t at_get_rb_data(char *buf, uint32_t size)
{
    int16_t result = Q_EOK;

    assert_param(buf);
	assert_param(size > sizeof(buf));

    if (ringbuffer_data_len(&g_uart_rxcb) >= size)
    {
		ringbuffer_getstr(&g_uart_rxcb, (uint8_t* )buf, size);
    }
    else
    {
        result = Q_ERROR;
    }
    return result;
}
/**
 * @description: 从rx_rb里面提取数据
 * @param: none
 * @return: 执行结果码
 */
static int at_recv_readline(void)
{
    static uint8_t s_recv_end_flag;
    uint16_t read_len = 0;

    char ch = 0, last_ch = 0;
    char is_full = 0;

    memset(g_at_info.recv_line_buf, 0x00, g_at_info.recv_bufsz);
    g_at_info.recv_line_len = 0;

    // 接收到期待的应答结果
    if (get_uart_recv_sta())
    {
        s_recv_end_flag = 1;
        uart_recv_sta_clean();
    }

    if (s_recv_end_flag != 1)
    {
        return Q_ERROR;
    }

    if ( 0 == ringbuffer_data_len(&g_uart_rxcb) )
    {
        s_recv_end_flag = 0;
        return Q_EMPTY;
    }
    /* 读到空 */
    while (ringbuffer_data_len(&g_uart_rxcb))
    {
        ringbuffer_getstr(&g_uart_rxcb, (uint8_t *)&ch,1);	//读取数据
        if (read_len < g_at_info.recv_bufsz)
        {
            g_at_info.recv_line_buf[read_len++] = ch;
            g_at_info.recv_line_len = read_len;
        }
        else
        {
            is_full = TRUE;
        }
        
        if ((ch == '\n' && last_ch == '\r') || get_urc_obj())
        {
            if (is_full)
            {
                LOG_D("read line failed. The line data length is out of buffer size(%d)!", g_at_info.recv_bufsz);
                memset(g_at_info.recv_line_buf, 0x00, g_at_info.recv_bufsz);
                g_at_info.recv_line_len = 0;
                return Q_EFULL;
            }
            break;
        }
        last_ch = ch;
    }
    return read_len;
}
/**
 * @description: AT/URC解析函数
 * @param: none
 * @return: none
 */
void at_parser(void)
{
    const struct at_urc *urc;

    /* 轮询串口数据标志位 */
    if (at_recv_readline() > 0)
    {
        /* 判断是否有urc以及urc匹配和对应handler执行 */
        if ((urc = get_urc_obj()) != NULL)
        {
            if (urc->func != NULL)
            {
                urc->func(g_at_info.recv_line_buf, g_at_info.recv_line_len);
            }
        }
        /* AT命令 响应 判断  */
        else if (g_at_info.resp != NULL)
        {
            at_response_t* resp = g_at_info.resp;

            /* 当前接收是响应 */
            g_at_info.recv_line_buf[g_at_info.recv_line_len - 1] = '\0';
            if (resp->buf_len + g_at_info.recv_line_len < resp->buf_size)
            {
                /* 复制响应行，以“ \ 0”分隔 */
                memcpy(resp->buf + resp->buf_len, g_at_info.recv_line_buf, g_at_info.recv_line_len);

                /* 更新当前的响应信息，以及行数 */
                resp->buf_len += g_at_info.recv_line_len;
                resp->line_counts++;
            }
            else
            {
                g_at_info.resp_status = AT_RESP_BUFF_FULL; 
                LOG_D("Read response buffer failed. The Response buffer size is out of buffer size(%d)!", resp->buf_size);
            }
            /* 检查响应结果 */
            if ( 0 == memcmp(g_at_info.recv_line_buf, AT_RESP_END_OK, strlen(AT_RESP_END_OK))
                    && 0 == resp->line_num)
            {
                /* 根据响应结果获取结束数据，返回响应状态END_OK。 */
                g_at_info.resp_status = AT_RESP_OK;
            }
            else if (strstr(g_at_info.recv_line_buf, AT_RESP_END_ERROR)
                     || ( 0 == memcmp(g_at_info.recv_line_buf, AT_RESP_END_FAIL, strlen(AT_RESP_END_FAIL))))
            {
                g_at_info.resp_status = AT_RESP_ERROR;
                LOG_D("g_at_info.resp_status FAIL");
            }
			else if ((resp->line_counts == resp->line_num) && resp->line_num)
			{
				/* 通过响应行获取结束数据，返回响应状态END_OK。*/
				g_at_info.resp_status = AT_RESP_OK;
			}
            else
            {
                return;
            }
            g_at_info.resp = NULL;
            g_at_info.resp_notice = 1;
        }
        else
        {
            //LOG_I("unrecognized line: %.*s", g_at_info.recv_line_len, g_at_info.recv_line_buf);
        }
    }
}

/**
 * @description: at命令响应相关初始化
 * @param: line_num 指定待获取行数信息
 * @param: timeout 超时时间
 * @return: 执行结果码
 */
void at_init_resp(uint16_t line_num, uint32_t timeout)
{
    g_at_resp.buf_size = BUFF_SIZE_MAX;
    g_at_resp.line_num = line_num;
    g_at_resp.line_counts = 0;
    g_at_resp.timeout = timeout;

    memset(g_at_resp.buf,0,g_at_resp.buf_size);
}

/**
* @description: 向模组直接发送数据(弱函数,需用户实现) 
 * @param  	
 * @return none
 */
__weak void at_send_module(uint8_t* send_buf, uint16_t buf_len)
{
	/*注意：当需要回调时,不应修改此函数,at_send_module() 需要在用户文件中实现*/
	/* 该函数用于向模组直接发送数据 */
	LOG_E("please implement this function< at_send_module() > in the user program!");
}

/**
 * @description: at命令format发送
 * @param: 
 * @param: 
 * @return 当前执行AT长度信息
 */
uint16_t at_vprintfln(const char *format, va_list args)
{
    g_last_cmd_len = vsnprintf(g_send_buf, sizeof(g_send_buf), format, args);

    strcat(g_send_buf,"\r\n");

    g_last_cmd_len = g_last_cmd_len+2;
	
	at_send_module((uint8_t* )g_send_buf,g_last_cmd_len);

    return g_last_cmd_len;
}
/**
 * @description: 获取当前发送的at_cmd
 * @param: cmd_size at_cmd大小
 * @return: 当前已发送的at_cmd
 */
const char *at_get_cmd(uint16_t *cmd_size)
{
    *cmd_size = g_last_cmd_len;
    return g_send_buf;
}

/**
 * @description: 发送at_cmd并阻塞等待响应
 * @param: cmd_expr at_cmd表达式
 * @return: 执行结果码
 */
int at_exec_cmd(const char *cmd_expr, ...)
{
    va_list args;
    uint16_t cmd_size = 0;
    const char *cmd = NULL;
    int16_t result = Q_EOK;
    uint32_t tick_start_times;

    assert_param(cmd_expr);

    // 上锁
    if (0 == g_at_info.lock)
    {
        g_at_info.lock = 1;
        tick_start_times = HAL_GetTick();
    }
    else
    {
        LOG_D("Send AT CMD Running!\r\n");
        return Q_EBUSY;
    }

    g_at_info.resp_status = AT_RESP_OK;
    g_at_info.resp = &g_at_resp;

    g_at_info.resp->buf_len = 0;
    g_at_info.resp->line_counts = 0;

    g_at_info.resp_notice = 0;

    va_start(args, cmd_expr);
    at_vprintfln(cmd_expr, args);
    va_end(args);

    while(1)
    {
        at_parser();

        // 超时退出;
        if(HAL_GetTick() - tick_start_times > g_at_info.resp->timeout)
        {
            cmd = at_get_cmd(&cmd_size);
            LOG_D("execute command (%.*s) timeout (%d ticks)!\r\n", cmd_size, cmd, g_at_info.resp->timeout);
            g_at_info.resp_status = AT_RESP_TIMEOUT;
            result = Q_ETIMEOUT;
            
			g_at_info.resp = NULL;
			g_at_info.lock = 0;
			break;
        }
		
        if( 1 == g_at_info.resp_notice)
        {
            g_at_info.lock = 0;
            break;
        }
    }
    return result;
}
/**
 * @description: 通过行号获取一行AT响应
 * @param: resp_line 行数
 * @return: 返回响应数据
 */
const char *at_resp_get_line(uint16_t resp_line)
{
    char *resp_buf = g_at_resp.buf;
    char *resp_line_buf = NULL;
    uint16_t line_num = 1;

    if (resp_line > g_at_resp.line_counts || resp_line <= 0)
    {
        LOG_D("AT response get line failed! Input response line(%d) error!", resp_line);
        return NULL;
    }

    for (line_num = 1; line_num <= g_at_resp.line_counts; line_num++)
    {
        if (resp_line == line_num)
        {
            resp_line_buf = resp_buf;

            return resp_line_buf;
        }

        resp_buf += strlen(resp_buf) + 1;
    }
	
    return NULL;
}

/**
  *	@description: 此功能将显示响应信息。
  * @return: none
  */
static void at_show_resp_info(void)
{
    /* Print response line buffer */
    const char *line_buffer = NULL;

    for (uint16_t line_num = 1; line_num <= g_at_resp.line_counts; line_num++)
    {
        if ((line_buffer = at_resp_get_line(line_num)) != NULL)
            LOG_D("line %d buffer : %s\r\n", line_num, line_buffer);
        else
            LOG_D("Parse line buffer error!\r\n");
    }
}

/**
  * @description: 通过关键字获取一行AT响应缓冲区
  * @param: keyword 关键字查询关键字
  * @return: 响应行缓冲区
  */
const char *at_resp_get_line_by_kw(const char *keyword)
{
    char *resp_buf = g_at_resp.buf;
    char *resp_line_buf = NULL;
    uint16_t line_num = 1;

    assert_param(keyword);

    for (line_num = 1; line_num <= g_at_resp.line_counts; line_num++)
    {
        if (strstr(resp_buf, keyword))
        {
            resp_line_buf = resp_buf;

            return resp_line_buf;
        }

        resp_buf += strlen(resp_buf) + 1;
    }
    return NULL;
}



/**
 * @description: 通过关键字获取和解析AT响应缓冲区参数
 * @param: keyword 查询关键字
 * @param: resp_expr 响应缓冲区表达
 * @return: 获取行缓冲
 */
int at_resp_parse_line_args_by_kw(const char *keyword, const char *resp_expr, ...)
{
    va_list args;
    int resp_args_num = 0;
    const char *resp_line_buf = NULL;

    assert_param(resp_expr);

    if ( NULL == (resp_line_buf = at_resp_get_line_by_kw(keyword)) )
    {
        return -1;
    }

    va_start(args, resp_expr);

    resp_args_num = vsscanf(resp_line_buf, resp_expr, args);

    va_end(args);

    return resp_args_num;
}

/**
 * @description: 清除响应buffer
 * @return: none
 */
void at_clean_resp(void)
{
    memset(g_at_resp.buf,0,g_at_resp.buf_size);
}

/**
  * @description: 此功能将发送命令并检查结果。
  * @param: cmd 命令发送到客户端
  * @param: resp_expr 预期响应表达式
  * @param: lines 响应行(一般情况下不使用，默认为'0')
  * @param: timeout 等待时间
  * @return: 执行结果码
  */
int at_send_cmd(const char* cmd, const char* resp_expr,
                   const uint16_t lines, const uint32_t timeout)
{
    int result = 0;

    char resp_arg[AT_CMD_MAX_LEN] = { 0 };

    at_init_resp(lines, timeout);

    LOG_I("send AT:%s\r\n",cmd);
	
    result = at_exec_cmd(cmd);
    if (result < 0)
    {
        LOG_I("send AT commands failed or wait response timeout!\r\n");
        at_clean_resp();
		
		return result;
    }

    at_show_resp_info();

    if (resp_expr)
    {
        if (at_resp_parse_line_args_by_kw(resp_expr, "%s", resp_arg) <= 0)
        {
            LOG_D("# >_< Failed\r\n");
            result = Q_ERROR;
            at_clean_resp();
			return result;
        }
        else
        {
			result = Q_EOK;
            LOG_D("# ^_^ successed\r\n");
        }
    }
    return result;
}
