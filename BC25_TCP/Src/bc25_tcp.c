/****************************************************************************
*
* Copy right: 2020-, Copyrigths of Quectel Ltd.
****************************************************************************
* File name: bc25_tcp.c
* History: Rev1.0 2020-12-1
****************************************************************************/

#include "bc25_tcp.h"

#include "at_cmd.h"
#include "dbg_log.h"
#include "middleware.h"

#include "usart.h"
#include "gpio.h"

#include <string.h>
#include <stdio.h>

#define AT_QIOPEN			"AT+QIOPEN=%d,%d,\"%s\",\"%s\",%d,%d,%d"
#define AT_QICLOSE			"AT+QICLOSE=%d"
#define AT_QICFG          	"AT+QICFG=\"showlength\",1"

event_except_info_t g_except_info = EVENT_END;

tcp_param_t g_tcp_param = {	
	
	"220.180.239.212",			// IP 地址
	8203,						// 端口

	1, 							// context_id		
	0, 							// connect_id
	SOCKET_SERVICE_TYPE_TCP,	// TCP / UDP
	SOCKET_PROTOCOL_TYPE_IPV4,  // IPV4 / IPV6
	SOCKET_MODE_DIRECT_PUSH		// BUFFER模式 / Direct Push模式
};

static void qiopen_urc_handler(const char *data, uint16_t size);
static void qiurc_urc_handler(const char *data, uint16_t size);												

/**
 * @description: URC 匹配表
 */									
static const struct at_urc urc_table[] = {
    { "+QIOPEN:", "\r\n", qiopen_urc_handler},
	{ "+QIURC:", "\r\n", qiurc_urc_handler},
};

char recvData[1024]; // 接收缓存数据

/**
 * @description: +QIURC URC处理函数
 */	
static void qiurc_urc_handler(const char *data, uint16_t size)	
{
	int connectID = 0;
	int recv_length = 0;
	
	
	LOG_I("%s",data);
	
	/* URC待区分 */
	/*
		// 常用处理urc
		+QIURC: "closed",<connectID>   建议 业务未完成 继续QIOPEN 或者进入待机模式（SLEEP）
		+QIURC: "recv",<connectID>,<current_recv_length>,<data>
	
		// 可扩展urc
		+QIURC: "recv",<connectID>,"buff full"  缓存模式请增加处理该urc防止
		+QIURC: "incoming full"  下行连接达到上限
		+QIURC: "incoming",<connectID>,<serverID>,<remoteIP>,<remote_port>
	*/
	
	if(strstr(data,"closed"))
	{
		/* 连接断开 */
		set_except_event(QICLOSE_EVENT);
		LOG_I(" TCP Server close!");
	}
	else if(strstr(data,"recv"))
	{
		sscanf(data,"%*[^,],%d,%d",&connectID,&recv_length);
		
		/* 已有数据下发,需去ringbuffer中读取 */
		at_get_rb_data(recvData, recv_length);
		
		LOG_I("recvData:%d,%d,%s",connectID,recv_length,recvData);
	}
	else
	{

	}
}

/**
 * @description: +QIOPEN URC处理函数
 */	
static void qiopen_urc_handler(const char *data, uint16_t size)
{
	int connectID = 0;
	int err = 0;
	
	// +QIOPEN: 0,0  
	sscanf(data,"%*[^ ] %d,%d",&connectID,&err);
	
	LOG_D("connectID: %d,err: %d\r\n",connectID,err);
	
	if( 0 == err )
	{
		set_except_event(QIOPEN_CONNECT_EVENT_SUCC);
	}
	else /* 异常 eg: 556... */
	{
		/* 错误处理 */
		set_except_event(QIOPEN_CONNECT_EVENT_FAIL);
	}
}

/**
 * @description: 事件处理函数
 */
void set_except_event(event_except_info_t except_info)
{
	g_except_info = except_info;
}

/**
 * @description: 初始化函数
 * @param None
 * @return None 
 */
void bc25_init(void)
{
	at_init();;
	at_set_urc_table(urc_table, sizeof(urc_table) / sizeof(urc_table[0]));
}

/**
 * @description:  硬件复位模组
 * @param None
 * @return None
 */
void bc25_reset(void)
{	
	HAL_GPIO_WritePin(bc25_reset_pin_GPIO_Port, bc25_reset_pin_Pin, GPIO_PIN_SET);
	HAL_Delay(1000);	// 复位时间
	HAL_GPIO_WritePin(bc25_reset_pin_GPIO_Port, bc25_reset_pin_Pin, GPIO_PIN_RESET);
	HAL_Delay(3000);	// 等待模组复位	
}

/**
 * @description: Vbat电源控制
 * @param power_mode		POWER_ENABLE_MODE 		使能上电
 *							POWER_DISABLE_MODE		失能去电
 * @return None
 */
void bc25_power(enum_power_mode_t power_mode)
{	

	if(power_mode == POWER_DISABLE_MODE)
	{
		at_set_cfun(CFUN_DISABLE);
		HAL_Delay(1000);	
		HAL_GPIO_WritePin(bc25_powd_pin_GPIO_Port, bc25_powd_pin_Pin, GPIO_PIN_SET);
		HAL_Delay(1000);
	}

	if(power_mode == POWER_ENABLE_MODE)
	{
		HAL_GPIO_WritePin(bc25_powd_pin_GPIO_Port, bc25_powd_pin_Pin, GPIO_PIN_RESET);
		HAL_Delay(1000);

		HAL_GPIO_WritePin(bc25_powkey_pin_GPIO_Port, bc25_powkey_pin_Pin, GPIO_PIN_SET);
		HAL_Delay(1000);
		HAL_GPIO_WritePin(bc25_powkey_pin_GPIO_Port, bc25_powkey_pin_Pin, GPIO_PIN_RESET);
		HAL_Delay(1000);
	}
}

/**
 * @description: 模组连接,用于判断是否与模组建立连接
 * @param None
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t bc25_connect(void)
{	
	int8_t result = Q_EOK;
	
	int8_t count = 10;
	
	while(count > 0 && Q_EOK != at_send_cmd(AT_TEST, AT_OK, 0, 1000))
	{
		HAL_Delay(1000);
		count--;
	}
	if( 0 == count )
	{
		LOG_D("<-- connection bc25 module failed! -->");
		result = Q_ERROR;
	}
	return result;
}

/**
 * @description: 配置TCP参数
 * @param None
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t bc25_tcp_qicfg(void)
{
	int8_t result = Q_EOK;

	result = at_send_cmd(AT_QICFG, AT_OK, 0, 15000);
	if(result != Q_EOK)
	{
		LOG_I("check_send_cmd: %s is error,result: %d !",AT_QICFG, result);
	}
	return result;	
}

/**
 * @description: 打开TCP socket连接
 * @param None
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t bc25_tcp_qiopen(void)
{
	int8_t result = Q_EOK;
	char send_cmd[50];
	
    memset(send_cmd, 0, sizeof(send_cmd));

	if(g_tcp_param.service_type == SOCKET_SERVICE_TYPE_TCP)
	{
		/* AT+QIOPEN=1,0,"TCP","220.18.39.22",8203,0,0 */
		sprintf(send_cmd,"AT+QIOPEN=%d,%d,\"%s\",\"%s\",%d,%d,%d",g_tcp_param.context_id,g_tcp_param.connect_id,
														"TCP",g_tcp_param.ip,g_tcp_param.port,g_tcp_param.protocol_type,g_tcp_param.access_mode);
	}
	else if(g_tcp_param.service_type == SOCKET_SERVICE_TYPE_UDP)
	{
		/* AT+QIOPEN=0,1,"UDP","220.180.239.212",8203 */
		sprintf(send_cmd,"AT+QIOPEN=%d,%d,\"%s\",\"%s\",%d,%d,%d",g_tcp_param.context_id,g_tcp_param.connect_id,
														"UDP",g_tcp_param.ip,g_tcp_param.port,g_tcp_param.protocol_type,g_tcp_param.access_mode);
	}
	else
	{
		
	}
    result = at_send_cmd(send_cmd, AT_OK, 0, 3000);
		
	return result;
}

/**
 * @description: 关闭TCP socket连接
 * @param None
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t bc25_tcp_qiclose(void)
{
    int8_t result = Q_EOK;
	char send_cmd[50];

    memset(send_cmd, 0, sizeof(send_cmd));

	sprintf(send_cmd,AT_QICLOSE,g_tcp_param.connect_id);

    result = at_send_cmd(send_cmd, AT_OK, 0, 3000);

	return result;
}

/**
 * @description: 发送数据
 * @param None
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t bc25_tcp_send(uint8_t* data,uint16_t len)
{
	int8_t result = Q_EOK;
	char send_cmd[50]={0};
	uint8_t endData = 0x1A;
	
//	AT+QISEND=0,10,"1234567890"
		
	sprintf(send_cmd,"AT+QISEND=%d",g_tcp_param.connect_id);
		
	result = at_send_cmd(send_cmd, ">", 2, 3000);
	
	at_send_module(data,len);
	at_send_module(&endData,1);

	return result;
}
