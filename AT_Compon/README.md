## STM32

### STM32 串口全双工原子锁问题
​		在使用STM32的HAL库开发时候,在使用UART时,偶尔会碰到突然不再接收数据的情况.调试发现,信号有的,但是就是软件不再进入接收中断了.通过调试,最后定位到问题点在于__HAL_LOCK()这个函数里。原因是串口是全双工的，但是会触发原子锁，在快速同时接收的时候可能发生被锁的情况，导致现象为:   mcu运行正常，业务逻辑可正常处理，但是不再触发接收中断，导致无法接收串口数据。

​	解决以上问题，需要注释一下代码；该代码均属于HAL库部分。

```C
HAL_StatusTypeDef HAL_UART_Receive_IT(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size)
// 1259行 __HAL_LOCK(huart);
// 1269行 __HAL_UNLOCK(huart);

HAL_StatusTypeDef HAL_UART_Transmit(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout)
// 1041行 __HAL_LOCK(huart);
// 1090行 __HAL_UNLOCK(huart);
```

### 硬件部分使用情况

* UART1   =》 LOG日志打印 
* UART2   =》 模组通信
* TIM7  =》串口接收结束标记位（10ms）基准

### 关于框架移植说明

​		该程序在移植过程中，需移植以下.c.h文件：

```c
ringbuffer.c/ringbuffer.h	  // 环形buff，用于存储AT串口数据。当前配置为：2K，可根据具体需求具体配置
dbg_log.c/dbg_log.h			 // 用于debug打印，当前使用的UART2 
at.c/at.h					// AT命令-URC实现框架，用于发送、响应AT命令以及解析URC
at_cmd.c/at_cmd.h			 // 常用、通用AT命令实现、用户可根据具体需求来增删。
```

​		用户程序需要实现或调用以下函数：

```C
/* 需用户实现,该函数用于启动TIM定时器 */
void tim_start_count(void);
/* 需用户实现,该函数用于将TIM定时器计数清零 */
void tim_clear_count(void);
/* 需用户实现,该函数用于直接向串口发送数据 */
void at_send_module(uint8_t* send_buf, uint16_t buf_len);
/* 需用户调用,该函数用于标记TIM计数时间结束标记状态 */
void set_uart_recv_sta(void);
/* 需用户调用,该函数用于存储数据接收 */
void rb_data_putchar(const uint8_t ch);
/* 需用户调用,该函数用于AT命令、URC解析 */
void at_parser(void);
```

​		用户程序可根据需求，来决定是否使用以下.c.h文件：

```c
bc25_aep.c/bc25_aep.h					// 连接AEP平台相关的AT命令发送、响应以及URC解析接口
middleware.c/middleware.h				// 中间件，常见的数据处理等接口
AepServiceCodes.c/AepServiceCodes.h		 // AEP平台提供的数据模型
```

​		关于在MCU与模组数据交互中，AT命令或URC常常以"\r\n"结尾，但是当用户业务数据中包含"\r\n"时，在软件判断过程中就会出现误判等情况。为解决该问题，可以将原先以"\r\n"为一包数据结束的标记，更换成以数据前后两个字节到达的间隔时间过长作为结束标记位，这里设定的时间间隔为10ms，也就是说：当前一个字节和后一字节之间接收的时间间隔大于10ms，那么认为他们是两包数据。

### 关于log日志

* 日志分类

  ```c
  /* 打印应用信息 */
  /* LOG_I("quectel debug!"); -> [INFO][main():0148] quectel debug! */
  #define LOG_I(...) dbg_print("INFO", __FUNCTION__, __LINE__, __VA_ARGS__)
  /* 打印调试信息 */
  /* LOG_D("quectel debug!"); -> [DEBUG][main():0148] quectel debug! */
  #define LOG_D(...) dbg_print("INFO", __FUNCTION__, __LINE__, __VA_ARGS__)
  /* 打印ERR信息,可将字符串类型自动转为HEX数据*/
  /* LOG_E("123456"); -> [ERR][main():0148] 123456 */
  #define LOG_E(...) dbg_print("ERR", __FUNCTION__, __LINE__, __VA_ARGS__)	
  /* HEX 数据打印 */
  /* LOG_HEX("123456"); -> [HEX][123456][L:6] 0x31 0x32 0x33 0x34 0x35 0x36 */
  #define LOG_HEX(...) dbg_print_hex_buf("HEX", __VA_ARGS__)
  /* SEND_AT数据打印 */ 
  #define DBG_SEND_AT(...) dbg_print_buf("SEND_AT",__VA_ARGS__)
  ```

### 如何封装一个at命令?

* 不需要提取at答复的操作；

* 需要提取at答复内容的操作；

* 注意事项

  ```c
  /**
   \* @description: 此功能将发送命令并检查结果。
   \* @param: cmd 命令发送到客户端
   \* @param: resp_expr 预期响应表达式
   \* @param: lines 响应行
   \* @param: timeout 等待时间
   \* @return: 执行结果码
   */
  int at_send_cmd(const char* cmd, const char* resp_expr,const uint16_t lines, const uint32_t timeout)
  
  
   /*********************************** 直接执行类 **************************************/
  /**
   * @description: 关闭回显
   * @param: none
   * @return 执行结果码
   */
  #define ATE     	"ATE0"
  int8_t at_turn_off_echo(void)
  {
  	int8_t result = Q_EOK;
  	result = at_send_cmd(ATE, AT_OK, 0, 1000);
  	if(result != Q_EOK)
  	{
  		LOG_I("check_send_cmd: %s is ERROR,result: %d !",ATE, result);
  	}
  	return result;	
  }
  
   /*********************************** 带参数直接执行类 *******************************/
  /**
   * @description: 设置UE功能
   * @param: cfun_mode cfun模式		 CFUN_ENABLE 	 打开射频
   * 							     CFUN_DISABLE	 关闭射频
   * @return 执行结果码
   */
  #define AT_CFUN     	"AT+CFUN=%d"
  int8_t at_set_cfun(enum_cfun_mode_t cfun_mode)
  {
  	char send_cmd[50];			// 用于存放拼装的at命令缓存
  	int8_t result = Q_EOK;		// 返回结果
  	
  	sprintf(send_cmd,AT_CFUN,cfun_mode);
      // 采用宏定义方式，使用sprintf拼装str字符串 eg: AT+CFUN=%d  -> AT+CFUN=1/AT+CFUN=0 
      // sprintf(send_cmd,"AT+CFUN=%d",cfun_mode);	
  
      // 将组装成的字符串通过“at_send_cmd” 发送出去
  	result = at_send_cmd(send_cmd, AT_OK, 0, 90000);
  	if(result != Q_EOK)
  	{
  		LOG_I("check_send_cmd: %s is ERROR,result: %d !",send_cmd, result);
  	}
  	return result;
  }
  // 建议类似的at命令参数，全部封装成枚举类型。1、通过名称就可判断参数功能。2、可规避其他非限定参数
  
   /*********************************** 需提取at命令答复类 ******************/
  /**
    * @description: 通过关键字获取一行AT响应缓冲区
    * @param: keyword 关键字查询关键字
    * @return: 响应行缓冲区
    */
  const char *at_resp_get_line_by_kw(const char *keyword)
  
  /**
   * @description: 查询扩展信号质量
   * @param: none
   * @return 执行结果码
   */
  int8_t at_query_cesq(at_cesq_info_t* cesq_info)
  {
  	int8_t result = Q_EOK;
  	const char* tmp_str = NULL;
  	
  	result = at_send_cmd(AT_CESQ, AT_OK, 0, 1000);
  	if(result != Q_EOK)
  	{
  		LOG_I("check_send_cmd: %s is ERROR,result: %d !",AT_CESQ, result);
  	}	
  	else
  	{
  		memset(cesq_info,0,sizeof(at_cesq_info_t));
  		/* 该指针数据在下次发送at之前会被释放，所以获取到之后，需立即转存 */
  		tmp_str = at_resp_get_line_by_kw("+CESQ: ");
  		if(tmp_str != NULL)
  		{
              // 采用正则表达式获取对应参数
  			sscanf(tmp_str,"%*[^ ] %d,%d,%d,%d,%d,%d",
  				&cesq_info->rxlev,&cesq_info->ber,&cesq_info->rscp,&cesq_info->ecno,&cesq_info->rsrq,&cesq_info->rsrp);
  			LOG_I("%d,%d,%d,%d,%d,%d",cesq_info->rxlev,cesq_info->ber,cesq_info->rscp,cesq_info->ecno,cesq_info->rsrq,cesq_info->rsrp);
  		}
  	}
  	return result;
  }
  ```

  

### 如何封装URC?

* 普通封装
* 复杂数据封装
* 注意事项

```c
/**
 * @description: 建立URC 匹配表
 */
static const struct at_urc urc_table[] = {
	{ "+NNMI:", "\r\n", nnmi_urc_handler},
	{ "+QATSLEEP:", "\r\n", deepsleep_urc_handler},
};

/**
 * @description: at相关初始化
 * @param: urc_table urc匹配表
 * @param: table_sz 匹配表大小
 * @return: 执行结果码
 */
int at_set_urc_table(const struct at_urc *urc_table, uint16_t table_sz)
 
// 如果数据前缀中有\r\n。可以直接通过该函数获取数据。
/**
 * @description: 从rx_ringbuffer中获取数据
 * @param: buf 待存放buffer
 * @param: size 指定获取数据大小
 * @return: 执行结果码
 */
int8_t at_get_rb_data(char *buf, size_t size)
    
/**
 * @description: +NNMI URC处理函数
 */
static void nnmi_urc_handler(const char *data, uint16_t size)	
{
	char tmp_data1[100];
	char tmp_data2[100];
	set_run dest;
	
	LOG_I("data:%s",data);
	
	get_str((char* )data,tmp_data1,0);
	LOG_I("testdata1: %s",tmp_data1);
	
	get_str((char* )data,tmp_data2,1);
	LOG_I("testdata2: %s",tmp_data2);
	
	// 061F4100250009010000000000000015
	if(set_run_DecodeCmdDown (tmp_data2+14, &dest) == AEP_CMD_SUCCESS)
	{
		LOG_I("*******************************************************");
		LOG_I("onoff_state: %d",dest.onoff_state);
		LOG_I("mode: %d",dest.mode);
		LOG_I("current_speed: %d",dest.current_speed);
		LOG_I("fan_up_down: %d",dest.fan_up_down);
		LOG_I("fan_left_right: %d",dest.fan_left_right);
		LOG_I("temperature: %d",dest.temperature);
		LOG_I("*******************************************************");
	}
}
```

### 逻辑思想

* void work_state(void) ；  =》  业务逻辑状态机；（用于客户业务逻辑处理）				
* void at_parser(void );       =》  AT-URC解析函数；（用于解析AT_SERVER应答、URC解析）
* void exception_event_callback(void)    =》   状态监控函数;(由异常callback衍生而来，用于整体状态转换)

```c
work_state();   			// 业务逻辑处理状态机   主要用于处理客户业务逻辑
at_parser();    			// 用于AT-URC解析	  必须调用，解析模组通信数据
exception_event_callback();  // 事件处理状态机		 用于调度所有业务的状态
```

